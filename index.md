---
layout: default
title: Home
nav_order: 1
description: "The Digital Privacy DeCal is a student-facilitated course at UC Berkeley."
permalink: /
---

# Digital Privacy [DeCal](https://decal.berkeley.edu/) at UC Berkeley, Spring 2023

## Announcements

{% assign announcements = site.announcements | reverse %}
{% for announcement in announcements %}
{{ announcement }}
{% endfor %}

## Calendar

{% for module in site.modules %}
{{ module }}
{% endfor %}
