---
layout: page
title: Staff
description: A listing of all the course staff members.
---

# Staff

Add `berkeley.edu` or `protonmail.com` to the end of all emails.

## Facilitator

{% assign instructors = site.staffers | where: 'role', 'Facilitator' %}
{% for staffer in instructors %}
{{ staffer }}
{% endfor %}

{% assign teaching_assistants = site.staffers | where: 'role', 'Infrastructure' %}
{% assign num_teaching_assistants = teaching_assistants | size %}
{% if num_teaching_assistants != 0 %}
## Infrastructure

{% for staffer in teaching_assistants %}
{{ staffer }}
{% endfor %}
{% endif %}
