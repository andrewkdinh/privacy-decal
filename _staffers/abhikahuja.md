---
name: Abhik Ahuja
role: Facilitator
email: ahujaabhik@
website: https://abhikahuja.com
photo: profile-abhikahuja.jpeg
---

Hi! I’m a fourth year CS and Math major. I love Rubik’s cubes, origami, and chess. Feel free to email if you want to talk about any of those (or anything else) and I hope you enjoy this course! :)
