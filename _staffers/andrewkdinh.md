---
name: Andrew Dinh
role: Infrastructure
email: andrewkdinh@
website: https://andrewkdinh.com
photo: profile-andrewkdinh.jpeg
---

Andrew previously facilitated the DeCal. He has since graduated and started working at Apple.

<!-- [Schedule an appointment](#){: .btn .btn-outline } -->
