# Digital Privacy DeCal Website

Contains the source code for the Digital Privacy DeCal website taught at UC Berkeley.

## License

This site uses [Just the Docs](https://github.com/pmarsceill/just-the-docs), a documentation theme for Jekyll which is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

All code is licensed under [AGPL 3.0+](https://opensource.org/licenses/AGPL-3.0).

All website content is licensed under a [Creative Commons Attribution-NonCommercial 4.0 International License (CC BY-NC 4.0)](https://creativecommons.org/licenses/by-nc/4.0/).
