---
layout: default
title: Activity 1
nav_exclude: true
---

# Perspectives on privacy

For this activity, we will examine websites related to privacy. Note that UC Berkeley students have free access to NYT (which can be set up [here](https://nytimesineducation.com/access-nyt/)). Also, you can access cached versions of many articles with [this site](https://12ft.io/).

1. First, read the paragraph from [this article](https://www.newyorker.com/magazine/2020/10/26/taking-back-our-privacy) about Moxie Marlinspike beginning with "Enforcing laws, Marlinspike believes, should be difficult."

1. Then, read Shoshanah Zuboff's response to the question "Why does it matter that these big tech companies can mine personal data for profit?" in [this interview](https://www.nytimes.com/2021/05/21/technology/shoshana-zuboff-apple-google-privacy.html).

1. Consider these questions and prepare to share with the class:
- What do you think of each excerpt?
- Why is privacy important to you?