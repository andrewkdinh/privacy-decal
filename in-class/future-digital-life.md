---
layout: default
title: The future of digital life
nav_exclude: true
---

# Perspectives on the future of digital life

For this activity, we will examine websites related to the future of digital life and respond to a prompt on Excalidraw. The list below is intended as a starting point; you are welcome to browse elsewhere.

- [Declaration of Digital Autonomy](https://techautonomy.org/)
- [A New Digital Manifesto](https://anewdigitalmanifesto.com/)
- [The Future Will be Technical: A modular essay about our optimistic future](https://coolguy.website/the-future-will-be-technical/index.html)
- [web0 manifesto](https://web0.small-web.org/)
- [Towards a Digital Pluriverse](https://pluriverse.world)
- [DWeb Principles](https://getdweb.net/principles/)
- [The Privacy Pledge](https://privacy-pledge.com/)
- [Ethical design manifesto](https://ind.ie/ethical-design/)
- [Our World in Data: Internet](https://ourworldindata.org/internet)
- [(we)bsite](https://we-b.site/)
- [Yesterweb's list of Internet manifestos](https://yesterweb.org/manifesto/)
