---
layout: slides
title: Week 1
description: The presentation for the first week
nav_exclude: true
theme: black
transition: slide
---

# Digital Privacy DeCal
## Fall 2021
### Lecture 1

nnn

## Staff

nnn

## Andrew Dinh - DeCal Facilitator

- 3rd year CS student
- Interned at Apple this summer
- Interested in photography, calligraphy, ergonomics, sad music
- https://andrewkdinh.com
- Why privacy matters to me: I'm a private person in general, and I hate the idea of being manipulated

nnn

## Abhik Ahuja - TA
- 3rd year CS and Math student
- Some of my random interests: origami, rubik's cubes, photography
- Why privacy matters to me: Don't like the feeling of being spied on
- https://abhikahuja.com

nnn

## Zachary Zollman - TA

- 4th year molecular environmental bio student
- Interests: veganism, climate activism, reading
- Why privacy matters to me: experimentation is a precursor to change, people can face persecution based on their identity groups and beliefs so privacy can have safety implications
- An example of privacy-respecting software I like: ProtonMail

nnn

## Zachary Zollman - TA

- 4th year molecular environmental bio student
- Interests: veganism, climate activism, reading
- Why privacy matters to me: experimentation is a precursor to change, people can face persecution based on their identity groups and beliefs so privacy can have safety implications
- An example of privacy-respecting software I like: ProtonMail

nnn

## Logistics: lecture

- Course website: https://privacy-decal.com
- 2-unit course
- Thursdays 6:30-8 PM online
  - Most days will probably be ended early except for guest lecturers
  - Lectures will eventually be on Jitsi Meet

nnn


## Logistics: workload

- Small assignments each week designed to get you comfortable with using and evaluating various privacy-respecting software, submitted via bCourses
  - Doesn't require any advanced technical knowledge except for optional assignments
- In-class demo activities
- Final written paper (300 words) or alternative format about any subject relevant to class
- Questions?

nnn

## Course preview /
## topic progression

- Based on syllabus, generally examining different categories of digital life each week and discussing options that are currently available

nnn

## What this course is/isn't

- It *isn't* teaching you to be 100% private, just revealing the ways people can track you and general tools to protect against it
  - Everyone has their own comfort level with privacy, we want you be able to be informed enough to choose for yourself
- It *is* about tradeoffs between security, privacy, and ease of use

nnn

## What this class is/isn't (cont.)

- Things may get technical (curse of knowledge), we want people to feel empowered to ask questions/discuss/share
- Maximum privacy isn't our common goal, rather to equip ourselves with tools and knowledge to make more informed decisions for ourselves
  
nnn

## Our take on strategies for advancing privacy

- Structural change and individual action are both necessary
- No magic bullet 
  - [Swiss Cheese model](https://www.nytimes.com/2020/12/05/health/coronavirus-swiss-cheese-infection-mackay.html), shared and personal responsibilities 
- When working to advance human rights, no permanent allies, no permanent enemies

Note:
- Individual choices impact other people, but only so much
- but the more layers the better like with fighting COVID-19

 
nnn

## Outcomes/non-outcomes

- Outcomes of this class:
  - Understand how software is used to track users across the web
  - Be knowledgeable of tools to prevent tracking
  - Be familiar with privacy-respecting alternatives to popular websites and services
- Non-outcomes of this class:
  - Hiding under a rock
  - Disconnecting from the internet and becoming a hermit

nnn

## Why "Digital" Privacy?

- "Privacy" is too encompassing
- "Online privacy" doesn't cover encrypted hard drives, etc.

nnn

## Intro to Privacy

nnn

## Privacy

- What is it?
- Privacy, security, and anonymity (per CS 161)
  - Privacy: Protecting data from unauthorized access
  - Security: Enforcing a desired property in the presence of an attacker
    - Data confidentiality, user privacy, data and computation integrity, authentication, availability, etc.
  - Anonymity: Person's identity is unknown
<!-- Insert dictionary definitions for each -->

- Privacy as mindfulness, self-care
- Privacy as self-determination, autonomy, freedom, human right, etc.
- Concept of [user freedom](https://web.archive.org/web/20210123004241/http://deblanc.net/blog/2018/12/22/user-freedom-n/)
<!-- http://deblanc.net/blog/2018/12/22/user-freedom-n/ -->

- Related content: [Libre Lounge Episode 4: The Universal Declaration of Human Rights and Free Software](https://librelounge.org/episodes/episode-4-the-universal-declaration-of-human-rights-and-free-software.html)

nnn

## What data you might want to keep private/the value of data

- Health records, salary, location data, affiliations (political beliefs, religion), gender, sexuality, activism and anti-establishment activities in general, financial and identity information like social security number
- Has value bc can be used to predict behavior, such as shopping habits, and also to manipulate behavior, including group behavior like voting patterns

nnn

## The Internet

- Before, it was harder to communicate and easier to keep things private
- The internet connected the world, in both positive and negative ways
- In the beginning, people were just trying to communicate with each other in any way possible
  - Generally, security/privacy has been an afterthought
- Just because on internet, doesn't mean all privacy gone
  - More of a spectrum

nnn 

## Current state of the web

- Entire businesses based around gathering and selling your data
- Tech giants that know what you're doing at any point in time
- Google made [$147 billion](https://www.cnbc.com/2021/05/18/how-does-google-make-money-advertising-business-breakdown-.html) in ad revenue in 2020

nnn

## Snowden who?

- After 9/11, US Gov passed PATRIOT ("Providing Appropriate Tools Required to Intercept and Obstruct Terrorism") Act to give more surveillance powers to law enforcement
  - Could tap domestic/international phones
- Edward Snowden: whistleblower who revealed that NSA is spying on everybody
  - XKeyscore (Google for finding information on anybody)
- Before that point, people thought only spying on terrorists, but anybody can be considered a terrorist, [including nonviolent human rights advocates like MLK](https://www.thenation.com/article/culture/mlk-fbi-sam-pollard/)
- International intelligence alliances: Five/Nine/Fourteen Eyes

nnn

## Panopticon

- A tower, canonically [at a prison](https://commons.wikimedia.org/wiki/File:Panopticon.jpg), can allow everyone in view to be observed by a single security guard, without the inmates being able to tell whether they are being watched
  - [The Feeling of Being Watched](https://www.feelingofbeingwatched.com/) directed by Assia Boundaoui
- Even the potential for surveillance can have psychological/behavioral impacts
- Ambiguity around surveillance can benefit the surveillor

nnn 


## Surveillance capitalism

<!-- Zach -->

- [Wikipedia](https://en.wikipedia.org/wiki/Surveillance_capitalism): "an economic system centered around the commodification of personal data with the core purpose of profit-making... collecting and processing data in the context of capitalism's core profit-making motive might present a danger to human liberty, autonomy, and wellbeing."
- Your data can be used to manipulate you to act in a certain way

## Shoshana Zuboff's definition
as presented in [The Age of Surveillance Capitalism](https://www.publicaffairsbooks.com/titles/shoshana-zuboff/the-age-of-surveillance-capitalism/9781610395694/](https://news.harvard.edu/gazette/story/2019/03/harvard-professor-says-surveillance-capitalism-is-undermining-democracy/)

1. A new economic order that claims human experience as free raw material for hidden commercial practices of extraction, prediction, and sales; 
2. A parasitic economic logic in which the production of goods and services is subordinated to a new global architecture of behavioral modification;
3. A rogue mutation of capitalism marked by concentrations of wealth, knowledge, and power unprecedented in human history;
4. The foundational framework of a surveillance economy;
5. As significant a threat to human nature in the twenty-first century as industrial capitalism was to the natural world in the nineteenth and twentieth;
6. The origin of a new instrumentarian power that asserts dominance over society and presents startling challenges to market democracy;
7. A movement that aims to impose a new collective order based on total certainty;
8. An expropriation of critical human rights that is best understood as a coup from above: an overthrow of the people’s sovereignty.

nnn

## Threat Model

- Identify what you want to keep private, as well as what actors you want to hide it from
  - One side is reveal everything to everybody
  - Other side is stop using internet to hide from everybody
  - somewhere in the middle is keeping mostly everything encrypted and on-device except say, posting a Tweet every once in a while

nnn

// <media-tag src="https://files.cryptpad.fr/blob/02/02ef596d3095e7ba00833e1c99b57e58d4850c073507c2b7" data-crypto-key="cryptpad:7F5UiWrl8zAbmmPTQ21lhhORBL/e4T251hFGDWNDDjU="></media-tag>



## Hands-on demo 🖍

- get out the crayons
- test Jitsi Meet
- join our [Mattermost](https://chat.privacy-decal.com)
