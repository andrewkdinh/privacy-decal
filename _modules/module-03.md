---
title: Where might we apply these strategies?
---

03/13
: Websites and browsers

03/20
: Security and digital footprint

04/03
: Guest lecture

04/10
: Real-time communication

04/17
: Operating systems

04/24
: Visions of a different Internet
: **Activity**{: .label .label-blue }
[The future of digital life](/in-class/future-digital-life)
