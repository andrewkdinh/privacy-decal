---
title: Where are we?
---

01/30
: Intro to digital privacy
: **Activity**{: .label .label-blue }
[Perspectives on privacy](/in-class/perspectives-on-privacy)

02/06
: Surveying the digital privacy landscape
  <!-- : [Solution](#) -->



<!-- 
09/05
: **HW 1 assigned**{: .label .label-blue }
-->

<!-- **Section**{: .label .label-purple } -->
