---
layout: page
title: Project
description: Project expectations and resources
---

# Project expectations and resources


**TL;DR: Submit a 300- to 500-word text you have written about exploring a topic in digital privacy by 04-24.**

This written assignment consists of exploring a topic related to digital privacy and writing about  your experience. The text of your submission should be 300 to 500 words in length. You are welcome to include images, audio, and other non-textual media as well. We plan to create a project gallery on the course website on which you can, optionally, display your work. The project counts for 20% of your grade in the course.


---------------------

## Project ideas


-   **Host** at least one privacy-respecting service and write a reflection about your experience.

    -   You can use our self-hosting guide (<https://privacy-decal.com/self-hosting/>) if you like.

-   **Contribute** time to a project related to digital privacy (such as privacy-related open-source software or technology-related Wikipedia articles) and write a reflection about your experience.

    -   You can identify software related to privacy through our resources page: [](https://github.com/pluja/awesome-privacy)[](https://privacy-decal.com/)<https://privacy-decal.com/resources/>[](https://privacy-decal.com/)[](https://github.com/pluja/awesome-privacy)

    -   Categories that could have pages that would be relevant to edit on Wikipedia include [Internet](https://en.wikipedia.org/wiki/Category:Internet), [software](https://en.wikipedia.org/wiki/Category:Software), [privacy](https://en.wikipedia.org/wiki/Category:Privacy), [new media artists](https://en.wikipedia.org/wiki/Category:New_media_artists), and [philosophy of technology](https://en.wikipedia.org/wiki/Category:Philosophy_of_technology). You could also contribute privacy-related content in other languages, to other Wikimedia projects, or potentially to other content projects with privacy implications.

-   Begin to **reimagine** software for privacy. Choose part of a program with significant privacy implications and clearly justify your selection. Then, outline an alternative design with comparable functionality that better reflects your desired privacy outcomes. How does the user interface guide user decisions with respect to privacy? How does the functionality affect the privacy of its users? Mockups and other visuals can be especially helpful for this area. (A good place to look for ideas could be <https://www.deceptive.design/> !)

-   **Investigate** a topic covered in this course and write about what you find. This could be presented as a research paper, interview, or even a detailed reflection.

    -   You may refer to the course overview and slides to review topics.

    -   You could investigate a category of services and compare the privacy implications of the available software. For example, you could investigate the range of consumer cloud storage services (such as Google Drive and iCloud Drive). You could use alternative software directories (such as privacytools.io) as a starting point in identifying software in a particular category.

    -   You could read a book as part of your investigation process.

        -   Books tagged "privacy" on Goodreads: <https://www.goodreads.com/shelf/show/privacy>



------------------------------

## Optional online availability

As an optional extension, you may submit a version of your text as a webpage or website. We have listed some resources for online content creation below.

-   Static site generators and other page creation tools
    -   <https://jamstack.org/generators/>
    -   <https://hotglue.me/start>
    -   <https://withknown.com/>
    -   <https://bearblog.dev/>
-   Repositories with web hosting
    -   GitHub
    -   GitLab
    -   Codeberg

--------------------------------------------

## Tentative criteria for evaluating projects

| Area                   | Full points                                                                                                                                                                                                  | Points available |
|------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------|
| Exploration            | explores something relevant to the course, goes beyond the main material of the course, demonstrates more detailed knowledge of the topic                                                                    | 6                |
| Response               | detailed and considered response; suggests that the student devoted time and energy to their work and developed substantive ideas; focuses on a coherent set of themes and ideas; communicates ideas clearly | 10               |
| Satisfies requirements | meets word count requirement; refers to sources appropriately, using a connsistent format such as MLA; uses at least 3 citations; uses formatting appropriately                                              | 4                |
