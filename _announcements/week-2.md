---
title: Week 2
week: 2
date: 2023-02-07
---

If you miss class and would like to request that your absence be excused, please log your absence on [the attendance form](https://tiny.cc/priv-decal-attendance).

February 8 is the last day for undergraduates to add a class via the enrollment center ([per the registrar](https://registrar.berkeley.edu/calendar/)).