---
layout: page
title: Resources
description: Resources
---


# Resources

## Privacy practice summaries

- [Terms of Service; Didn't Read](https://tosdr.org/)
- [Exodus Privacy](https://exodus-privacy.eu.org/en/)
- [Privacy Spy](https://privacyspy.org/)

## Software directories, lists, and guides
- [Privacy Guides](https://www.privacyguides.org/) and [their Reddit](https://www.reddit.com/r/PrivacySoftware/)
- [Privacy Tools](https://www.privacytools.io/) and [their Reddit](https://www.reddit.com/r/privacytoolsIO/)
- [switching.software](https://switching.software/)
- [AlternativeTo](https://alternativeto.net)
- [gofoss.net](https://gofoss.net/)
- [RestorePrivacy](https://restoreprivacy.com/)
- [ethical.net](https://ethical.net/resources/)
- [avoidthehack](https://avoidthehack.com/)
- [Below Radar](https://belowradar.co.uk/resources)
- [Awesome Privacy](https://github.com/pluja/awesome-privacy)
- [Surveillance Self-Defense](https://ssd.eff.org/en)
- [Security in a Box](https://securityinabox.org/en/)
- [Privacy Respecting](https://github.com/nikitavoloboev/privacy-respecting)
- [PRISM Break](https://prism-break.org/en/)
- [Big list of small web services](https://alex.flounder.online/tech/bigandsmall.gmi)
- [redecentralize/alternative-internet](https://github.com/redecentralize/alternative-internet)
- [awesome-selfhosted](https://github.com/awesome-selfhosted/awesome-selfhosted)


## Comparisons
### Browsers

- [privacytests.org](https://privacytests.org/)
- [avoidthehack's comparison](https://avoidthehack.com/util/browser-comparison)

### Messaging apps
- [securemessagingapps.com](https://www.securemessagingapps.com/)
- [Messenger Matrix](https://www.messenger-matrix.de/messenger-matrix-en.html)
- [Thilo Buchholz's post on messaging apps](https://thilobuchholz.de/2021/02/3-types-of-messengers-simply-explained/)
- [The Control Alt Delete Blog's post on messaging apps](https://controlaltdelete.technology/articles/the-importance-of-using-messaging-apps-with-end-to-end-encryption-which-ones-to-use-and-why.html)
